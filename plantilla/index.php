<?php defined( '_JEXEC' ) or die;

include_once JPATH_THEMES.'/'.$this->template.'/logic.php';

?><!doctype html>

<html lang="<?php echo $this->language; ?>">

<head>
	<jdoc:include type="head" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<link rel="apple-touch-icon-precomposed" href="<?php echo $tpath; ?>/images/apple-touch-icon-57x57-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $tpath; ?>/images/apple-touch-icon-72x72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $tpath; ?>/images/apple-touch-icon-114x114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $tpath; ?>/images/apple-touch-icon-144x144-precomposed.png">
	<link href="css/animate.css" rel="stylesheet" media="screen">
	<script type="text/javascript" src="js/jquery-2.2.1.min.js"></script>
	<script type="text/javascript" src="js/home.js"></script>
	<script type="text/javascript" src="js/jssor.slider.mini.js"></script>
	<!-- Le HTML5 shim and media query for IE8 support -->
	<!--[if lt IE 9]>
	<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<script type="text/javascript" src="<?php echo $tpath; ?>/js/respond.min.js"></script>
	<![endif]-->
</head>

<body class="<?php echo (($menu->getActive() == $menu->getDefault()) ? ('front') : ('site')).' '.$active->alias.' '.$pageclass; ?>">
	<div class="container">
  <section id="barra">
	  <div class="row">
	    <div class="col-md-2">
	      <jdoc:include type="modules" name="logo" />
	    </div>
	    <div class="col-md-10">
	      <jdoc:include type="modules" name="menu" />
	    </div>
	  </div>
		</section>
		<section id="top" data-stellar-background-ratio="0.5">
	  <div class="row">
	    <div class="col-md-12">
	      <jdoc:include type="component" />
	    </div>
	  </div>
	 </section>
	 <section id="slider">
	  <div class="row">
	    <div class="col-md-12">
	      <jdoc:include type="modules" name="slider" />
	    </div>
	  </div>
	</section>
	<section id="servicios">
	  <div class="row">
	    <div class="col-md-6">
	      <jdoc:include type="modules" name="productos" />
	    </div>
	    <div class="col-md-6">
	      <jdoc:include type="modules" name="servicios" />
	    </div>
	  </div>
	</section>
	<section id="chatapp">
	  <div class="row">
	    <div class="col-md-4">
	      <jdoc:include type="modules" name="chatapp" />
	      <jdoc:include type="modules" name="correotelefono" />
	    </div>
	    <div class="col-md-8">
	      <jdoc:include type="modules" name="mapa" />
	    </div>
	  </div>
	</section>
	<section id="pie">
	  <div class="row">
	    <div class="col-md-12">
	      <jdoc:include type="modules" name="pie" />
	    </div>
	  </div>
	</section>
	  <jdoc:include type="modules" name="debug" />

	</div>

</body>

</html>
